package challenge1_JSON;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.json.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Problem1 {
	 static String json = "./data/response.json";
	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		
		Map<String, Integer> statusCount1 =  prob1Func();
		for (Map.Entry entry : statusCount1.entrySet()) { 
            System.out.println(entry.getValue() +" documents are in "+ entry.getKey() +" state"); 
        }

	}
	public static Map prob1Func() throws IOException {

		Map<String, Integer> statusCount = new HashMap<String, Integer>();
		String text = new String(Files.readAllBytes(Paths.get(json)), StandardCharsets.UTF_8);
		JSONObject payload  = (JSONObject) new JSONObject(text).get("payload");
		JSONArray arr = payload.getJSONArray("items");
		
		for (int i = 0; i < arr.length(); i++) {
			String status = arr.getJSONObject(i).getString("status");
			if(statusCount.containsKey(status)) {
				statusCount.put(status, statusCount.get(status)+1);
			}
			else { 
				statusCount.put(status, 1); 
            } 
			
		}
		
		return statusCount;
	}

}
