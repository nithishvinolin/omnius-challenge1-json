package challenge1_JSON;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Problem3 {
	 static String json = "./data/response.json";
	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		
		JSONArray arr3 = prob1Func("10.pdf");
		for (int i=0;i<arr3.length();i++) {
			JSONObject jsonObject = arr3.getJSONObject(i);
			String document_id=jsonObject.getString("document_id");
			String collection_id=jsonObject.getString("collection_id");
			String status=jsonObject.getString("status");
			String file_name=jsonObject.getString("file_name");
			String created_date=jsonObject.getString("created_date");
			Integer revision_number=jsonObject.getInt("revision_number");
			System.out.println("document_id - "+document_id+"\n"+"collection_id - "+collection_id+"\n"
					+"status - "+status+"\n"
					+"file_name - "+file_name+"\n"
					+"created_date - "+created_date+"\n"
					+"revision_number - "+revision_number+"\n"
					);
			}

	}
	public static JSONArray prob1Func(String filenameArg) throws IOException {

		String text = new String(Files.readAllBytes(Paths.get(json)), StandardCharsets.UTF_8);
		JSONObject payload  = (JSONObject) new JSONObject(text).get("payload");
		JSONArray arr = payload.getJSONArray("items");
		JSONArray arr2 = new JSONArray();
		
		
		for (int i = 0; i < arr.length(); i++) {
			String file_name = arr.getJSONObject(i).getString("file_name");
			if (file_name.equalsIgnoreCase(filenameArg))
			{
				arr2.put(arr.getJSONObject(i));
			}
					
		}
		
	
		return arr2;
		
	}

}
